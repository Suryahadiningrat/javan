<?php

namespace App\Repository;

interface CRUDInterface {
    public function getAllData() : object;
    public function findData(int $id) : object;
    public function destroy(int $id) : bool;
    public function updateOrCreate(array $data) : object;
}