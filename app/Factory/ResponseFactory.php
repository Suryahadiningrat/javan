<?php

namespace App\Factory;

class ResponseFactory {

    public static function success(object $data, string $message = null) : object
    {
        return response()->json([
            'status' => 'success',
            'data' => $data,
            'message' => $message
        ]);
    }

    public static function condition(object $data, string $message = null, $code = null) : object
    {
        if ($data) {
            return response()->json([
                'status' => 'success',
                'data' => $data,
                'message' => $message
            ], $code ?? 200);
        } else {
            return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => $message,
                'errorCode' => '500'
            ], 200);
        }
    }
}