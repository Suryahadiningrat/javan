<?php

namespace App\Repository\Person;

use App\Repository\CRUDInterface;

interface PersonInterface extends CRUDInterface{
    public function getParentData() : object;
}