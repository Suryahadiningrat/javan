<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PersonController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1/persons')->group(function() {
    Route::get('all', [PersonController::class, 'all']);
    Route::get('{id}', [PersonController::class, 'find']);
    Route::post('create', [PersonController::class, 'create']);
    Route::put('update', [PersonController::class, 'update']);
    Route::delete('destroy/{id}', [PersonController::class, 'destroy']);
});
