@extends('layouts.app')

@section('title', isset($person) ? 'Edit' : 'Add')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ isset($person) ? 'Edit Person' : 'Create Person' }}</div>

                    <div class="card-body">
                        <a href="/">Back to Home</a>
                        <form id="itemFrom" role="form" method="POST"
                            action="{{ isset($person) ? '/update/' : '/create' }}">
                            @csrf
                            @isset($person)
                                @method('PUT')
                            @endisset

                            <div class="card-body">

                                @isset($person)
                                    <input type="hidden" name="id" value="{{ $person->id }}">
                                @endisset
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" value="{{ $person->name ?? '' }}" required>
                                </div>

                                <div class="form-group">
                                    <label>Gender</label>
                                    <select name="gender" required>
                                        <option value="m"
                                            {{ isset($person->gender) && $person->gender == 'Male' ? 'selected' : '' }}>
                                            Male
                                        </option>
                                        <option value="f"
                                            {{ isset($person->gender) && $person->gender == 'Female' ? 'selected' : '' }}>
                                            Female
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Parent</label>
                                    <select name="parent_id" required>
                                        @foreach ($parent as $item)
                                            <option value="{{ $item->id }}"
                                                {{ isset($person->parent_id) && $person->parent_id == $item->id ? 'selected' : '' }}>
                                                {{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">
                                    @isset($person)
                                        <i class="fas fa-arrow-circle-up"></i>
                                        <span>Update</span>
                                    @else
                                        <i class="fas fa-plus-circle"></i>
                                        <span>Create</span>
                                    @endisset
                                </button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
