<?php

namespace App\Http\Controllers\Api;

use App\Facades\Helper;
use Illuminate\Http\Request;
use App\Factory\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Repository\Person\PersonRepository;

class PersonController extends Controller
{
    protected $person;    

    public function __construct(PersonRepository $person)
    {
        $this->person = $person;
    }

    public function all()
    {
        $data = $this->person->getAllData();
        return ResponseFactory::success($data, "Success Get All Data");
    }

    public function find($id)
    {
        $data = $this->person->findData($id);
        return ResponseFactory::success($data, "Success Get Data");
    }
    
    public function create(Request $request)
    {
        $store = $request->only('name', 'parent_id', 'gender');
        $data = $this->person->updateOrCreate($store);
        return ResponseFactory::condition($data, "Create Data Persons", 201);
    }
    
    public function update(Request $request)
    {
        $store = $request->only('id', 'name', 'parent_id', 'gender');
        $data = $this->person->updateOrCreate($store);
        return ResponseFactory::condition($data, "Create Update Persons", 200);
    }

    public function destroy($id)
    {
        $isDeleted = $this->person->destroy($id);
        return ResponseFactory::success($id, "Success Delete Data");
    }
}
