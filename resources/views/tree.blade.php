@extends('layouts.app')

@push('styles')
    <link href="{{ asset('css/tree.css') }}" rel="stylesheet">
@endpush
@section('content')
    <section class="treeMainContainer">
        <div class="treeContainer d_f">
            <div class="_NewBranch d_f">
                @foreach ($persons as $key => $item)
                    @if (!$item->is_has_parent)
                        <div class="_treeRoot d_f">
                            <div class="_treeBranch hasChildren">
                                <div class="_treeRaw d_f">
                                    <div class="_treeLeaf d_f">
                                        <div class="t_Data d_f">
                                            <p class="shortName"> {{ $item->name }} </p>
                                        </div>
                                    </div>
                                </div>
                                @php unset($persons[$key]);@endphp
                                {!! \App\Facades\Helper::drawTreeBranch($persons, $item->id) !!}
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection
