<?php

namespace App\Facades;

class Helper {

    public function condReturnRedirectBack($data, string $message)
    {
        if ($data) {
            return redirect()->back()->with('success', 'Success ' . $message);
        } else {
            return redirect()->back()->with('danger', 'Failed ' . $message);
        }
    }

    public static function drawTreeBranch($persons, $parent_id)
    {
        $branches = "<div class='_NewBranch d_f'>";

        foreach ($persons as $key => $item) {
            if ($item->parent_id == $parent_id) {
                unset($persons[$key]);
                $isHasChildren = !$item->is_has_children ? null : 'hasChildren';
                $html = !$item->is_has_children ? '' : self::drawTreeBranch($persons, $item->id);
                
                $branches .= "<div class='_treeRoot d_f'>
                    <div class='_treeBranch $isHasChildren'>
                        <div class='_treeRaw d_f'>
                            <div class='_treeLeaf d_f'>
                                <div class='t_Data d_f'>
                                    <p class='shortName'> $item->name </p>
                                </div>
                            </div>
                        </div>
                        $html
                    </div>
                </div>";
            }
        }
        $branches .= "</div>";
        return $branches;
    }

}
