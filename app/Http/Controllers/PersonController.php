<?php

namespace App\Http\Controllers;

use App\Facades\Helper;
use Illuminate\Http\Request;
use App\Http\Requests\StorePersonRequest;
use App\Repository\Person\PersonRepository;

class PersonController extends Controller
{
    protected $person;    

    public function __construct(PersonRepository $person)
    {
        $this->person = $person;
    }

    public function index()
    {
        $data = $this->person->getAllData();
        return view('index', ['data' => $data]);
    }

    public function add()
    {
        $data['parent'] = $this->person->getParentData();
        return view('form', $data);
    }

    public function edit($id)
    {
        $data['parent'] = $this->person->getParentData($id);
        $data['person'] = $this->person->findData($id);
        return view('form', $data);
    }
    
    public function create(StorePersonRequest $request)
    {
        $store = $request->only('name', 'parent_id', 'gender');
        $data = $this->person->updateOrCreate($store);
        return Helper::condReturnRedirectBack($data, "Insert Data");
    }

    public function tree()
    {
        $data['persons'] = $this->person->getTreeData();
        return view('tree', $data);
    }
    
    public function update(StorePersonRequest $request)
    {
        $store = $request->only('id', 'name', 'parent_id', 'gender');
        $data = $this->person->updateOrCreate($store);
        return Helper::condReturnRedirectBack($data, "Update Data");
    }

    public function destroy($id)
    {
        $isDeleted = $this->person->destroy($id);
        return Helper::condReturnRedirectBack($isDeleted, "Delete Data");
    }
}
