<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PersonController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PersonController::class, 'index']);
Route::get('/tree', [PersonController::class, 'tree']);
Route::get('/add', [PersonController::class, 'add']);
Route::get('/edit/{id}', [PersonController::class, 'edit']);
Route::post('/create', [PersonController::class, 'create']);
Route::put('/update', [PersonController::class, 'update']);
Route::delete('/delete/{id}', [PersonController::class, 'destroy']);