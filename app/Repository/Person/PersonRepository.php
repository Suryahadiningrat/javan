<?php

namespace App\Repository\Person;

use App\Models\Person;
use DB;

class PersonRepository implements PersonInterface {

    public function getAllData() : object {
        return Person::latest()->get();
    }

    public function findData(int $id) : object {
        return Person::find($id);
    }

    public function getParentData(int $userId = null) : object {
        return Person::when($userId, function ($query) use ($userId) {
            $query->where('parent_id', '<>', $userId);
        })
        ->latest()
        ->get();
    }
    
    public function getTreeData() : object {
        return Person::select('id', 'parent_id', 'name', 
            DB::raw('(SELECT IF(count(id) > 0, TRUE, FALSE) FROM persons AS p WHERE p.parent_id = persons.id) AS is_has_children'), 
            DB::raw('IF(parent_id, TRUE, FALSE) AS is_has_parent'))
        ->latest()
        ->get();
    }

    public function updateOrCreate(array $data) : object {
        $id = $data['id'] ?? null;
        return Person::updateOrCreate(['id' => $id], $data);
    }


    public function destroy(int $id) : bool {
        return Person::find($id)->delete();
    }
}