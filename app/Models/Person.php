<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;
    protected $table = 'persons';
    
    protected $fillable = [
        'parent_id', 'name', 'gender'
    ];
    
    protected function getParentNameAttribute($value)
    {
        return self::select('name')->where('id', $this->parent_id)->first()->name ?? '-';
    }
    
    protected function getGenderAttribute($value)
    {
        return $value == 'm' ? 'Male' : 'Female';
    }

    protected function getCreatedAtAttribute($value)
    {
        return date('d, M Y', strtotime($value));
    }

    protected function getUpdatedAtAttribute($value)
    {
        return date('d, M Y', strtotime($value));
    }
}
