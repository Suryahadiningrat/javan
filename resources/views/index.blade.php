@extends('layouts.app')

@section('title', 'Index')

@section('content')
    <a href="/tree">Tree Family</a>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Index</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="/add" title="Create a new person"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>Name</th>
            <th>Parent</th>
            <th>Gender</th>
            <th>Date Created</th>
            <th>Date Updated</th>
            <th>Actions</th>
        </tr>
        @foreach ($data as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{{ $item->parent_name }}</td>
                <td>{{ $item->gender }}</td>
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->updated_at }}</td>
                <td>
                    <form action="/delete/{{ $item->id }}" method="POST">
                        <a href="/edit/{{ $item->id }}">
                            <i class="fas fa-edit fa-lg"></i>
                        </a>

                        @csrf

                        @method('DELETE')

                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

@endsection